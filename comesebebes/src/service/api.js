import React from 'react'
import axios from 'axios'

const create = axios.create({
    baseURL:'https://comesandbebes.herokuapp.com/'
})

async function getRestaurantList(setRestaurantList) {
    axios.get('https://comesandbebes.herokuapp.com/user/list_restaurants')
    .then((restaurants) => {
        setRestaurantList(restaurants['data'])
    })
    .catch((err)=>{
        console.error(err)
    })
}

export {getRestaurantList, create}