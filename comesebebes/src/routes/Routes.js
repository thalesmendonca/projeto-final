import React from 'react'
import { BrowserRouter as Router,  Switch, Route, Redirect } from 'react-router-dom'
import StoreProvider from '../components/Store/Provider'
import RoutesPrivate from '../components/Routes/Private/Private'
import LandingPage from '../pages/LandingPage/LandingPage'
import LoginPage from '../pages/LoginPage/LoginPage'
import RegisterPage from '../pages/RegisterPage/ResgisterPage'

/***** user pages**** =*/
import UserLoggedPage from '../pages/UserLoggedPage/UserLoggedPage'
import UserEditProfile from '../pages/UserEditProfile/UserEditProfile'
import UserWallet from '../pages/UserWalletPage/UserWalletPage'


/********* restaurant pages********* */
import RestaurantPage from '../pages/RestaurantPage/RestaurantPage'
import RestaurantRegisterPage from '../pages/RestaurantRegisterPage/RestaurantRegisterPage'
import RestaurantEditProfilePage from '../pages/RestaurantEditProfilePage/RestaurantEditProfilePage'
import RestaurantManagement from '../pages/RestaurantManagement/RestaurantManagement'
import RestaurantOrdersPage from '../pages/RestaurantOrdersPage/RestaurantOrdersPage'
import RestaurantResultsPage from '../pages/RestaurantResultsPage/RestaurantResultsPage'
import RestaurantStockManagement from '../pages/RestaurantStockManagement/RestaurantStockManagement'

/******** deliveryman pages*********/
import DeliverymanPage from '../pages/DeliverymanPage/DeliverymanPage'
import DeliverymanWalletPage from '../pages/DeliverymanWalletPage/DeliverymanWalletPage' 

/********* admin pages ***********/
import AdminPage1 from '../pages/AdminUserList/AdminPage'
import AdminPage2 from '../pages/AdminDeliverymanList/AdminPage'
import AdminRestaurantOwnerRegister from '../pages/AdminRestaurantOwnerRegister/AdminRestaurantOwnerRegister'

function Routes() {
    return (
        <Router>
            <StoreProvider>
                <Switch>
                    <Route exact path ='/'>
                        <LandingPage />
                    </Route>
                    <Route exact path ='/login' component={LoginPage}>
                    </Route>
                    <Route exact path ='/register'>
                        <RegisterPage/>
                    </Route>
                    <RoutesPrivate exact path ='/user-logged' component={UserLoggedPage}/>

                    <RoutesPrivate exact path ='/user-edit-profile' component={UserEditProfile}/>
                    
                    <RoutesPrivate exact path ='/user-wallet' component={UserWallet}/>
                    
                    <Route exact path ='/restaurant-page'>
                        <RestaurantPage />
                    </Route>
                    <Route exact path ='/restaurant-register'>
                        <RestaurantRegisterPage />
                    </Route>
                    <Route exact path ='/restaurant-edit-profile'>
                        <RestaurantEditProfilePage />
                    </Route>
                    <Route exact path ='/restaurant-management'>
                        <RestaurantManagement />
                    </Route>
                    <Route exact path = '/restaurant-orders'>
                        <RestaurantOrdersPage />
                    </Route>
                    <Route exact path ='/restaurant-results'>
                        <RestaurantResultsPage />
                    </Route>
                    <Route exact path ='/restaurant-stock'>
                        <RestaurantStockManagement />
                    </Route>
                    <Route exact path ='/deliveryman-page'>
                        <DeliverymanPage />
                    </Route>
                    <Route exact path ='/deliveryman-wallet'>
                        <DeliverymanWalletPage />
                    </Route>
                    <Route exact path ='/admin-page-user-list'>
                        <AdminPage1 />
                    </Route>
                    <Route exact path ='/admin-page-deliveryman-list'>
                        <AdminPage2 />
                    </Route>
                    <Route exact path ='/admin-page-owner-register'>
                        <AdminRestaurantOwnerRegister/>
                    </Route>
                </Switch>
            </StoreProvider>
        </Router>
    )

}

export default Routes
