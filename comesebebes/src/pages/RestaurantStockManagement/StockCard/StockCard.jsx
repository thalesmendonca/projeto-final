import React, {useState} from 'react'
import {RiPencilFill} from 'react-icons/ri'
import coxinha from '../assets/coxinhadejaca.jpg'

import './StockCard.css'

function StockCard() {
    const [inStock, setInStock] = useState(0)

    const addStock = () => {
        setInStock(inStock + 1)
    }

    const decreasesStock = () => {
        setInStock(inStock - 1)
    }

    return (
        <div className='stockCard'>
            <div>
                <img src={coxinha} alt="" />
                <div className='description'>
                    <h4>Prato 01</h4>
                    <p>Descrição da opção/prato</p>
                    <p>Porção: Média</p>
                    <p>R$ 0,00</p>
                </div>
            </div>
            <div className='controls'>
                <div>
                    <button onClick={decreasesStock}>-</button>
                    <p>{inStock}</p>
                    <button onClick={addStock}>+</button>                    
                </div>
                <div className='pencilIcon'>
                    <RiPencilFill/>
                </div>
            </div>
        </div>
    )
}

export default StockCard
