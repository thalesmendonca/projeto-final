import React from 'react'
import HeaderRestaurantLogged from '../../components/HeaderRestaurantLogged/HeaderRestaurantLogged'
import TitlePages from '../../components/TitlePages/TitlePages'
import StockCard from './StockCard/StockCard'
import Footer from '../../components/Footer/Footer'

import './RestaurantStockManagement.css'

function RestaurantStockManagement() {
    return (
        <div>
            <HeaderRestaurantLogged />
            <div className='bigBox'>
                <TitlePages title='Gestão de estoque'/>
                <div className='scrollable'>
                    <StockCard />
                    <StockCard />
                    <StockCard />
                    <StockCard />
                    <StockCard />
                    <StockCard />
                    <StockCard />
                    <StockCard />
                    <StockCard />
                </div>
            </div>
            <Footer />
        </div>
    )
}

export default RestaurantStockManagement
