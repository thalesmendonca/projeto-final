import React from 'react'

import './AverageRate.css'

function AverageRate() {
    return (
        <div className='averageRate'>
            <h4>Avaliação Média Restaurante 1</h4>
            <div>
                <h4 className='rateValue'>4.1</h4>
                <div className='rate'>
                    <span class="fa fa-star checked"></span>
                    <span class="fa fa-star checked"></span>
                    <span class="fa fa-star checked"></span>
                    <span class="fa fa-star"></span>
                    <span class="fa fa-star"></span>
                </div>
            </div>
        </div>
    )
}

export default AverageRate
