import React from 'react'

import coxinha from './assets/coxinhadejaca.jpg'

import './SoldPlates.css'

function SoldPlates() {
    return (
        <div className='soldPlates'>
            <h4>Pratos mais vendidos</h4>
            <div className={'rowDiv','carousel'}>
                <i className='arrowLeft'></i>
                <img src={coxinha} alt="" />
                <i className='arrowRight'></i>
            </div>
            <p>1º Lugar: Prato 02</p>
        </div>
    )
}

export default SoldPlates
