import React from 'react'

import './TotalSales.css'

function TotalSales() {
    return (
        <div className='totalSales'>
            <h4>Valor Total em Vendas</h4>
            <div className='rowDiv'>
                <p className='cifrao'>$</p>
                <p>10.000</p>
            </div>
        </div>
    )
}

export default TotalSales
