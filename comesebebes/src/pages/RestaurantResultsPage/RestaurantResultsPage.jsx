import React from 'react'
import HeaderRestaurantLoggedPage from '../../components/HeaderRestaurantLogged/HeaderRestaurantLogged'
import TitlePages from '../../components/TitlePages/TitlePages'
import TotalSales from './TotalSales/TotalSales'
import AverageRate from './AverageRate/AverageRate'
import SoldPlates from './SoldPlates/SoldPlates'

import './RestaurantResultsPage.css'
import LoyalCostomers from './LoyalCostomers/LoyalCostomers'

function RestaurantResultsPage() {
    return (
        <div className='restaurantResultsPage'>
            <HeaderRestaurantLoggedPage />
            <div className='bigBox'>
                <TitlePages title='Resultados'/>
            </div>
            <div className='smallBox'>
                <div className='rowDiv'>
                   <TotalSales />
                   <AverageRate />
                </div>
                <div className='rowDiv'>
                    <SoldPlates />
                    <LoyalCostomers />
                </div>
            </div>
        </div>
    )
}

export default RestaurantResultsPage
