import React, {useState} from 'react'
import Button from '../../../components/Button/Button'
import {useHistory} from 'react-router-dom'

import './LoginForm.css'

function initialState() {
    return { email: '', password: '' };
}

function LoginForm({openPasswordRec}) {

    
    const [values, setValues] = useState(initialState)
    
    function onChange(event) {
        const { value, name } = event.target;
        setValues({
            ...values,
            [name]: value,
        });
    }
    
    function onSubmit(event){
        
    }

    const history = useHistory()
    
    return (
        <form onSubmit={onSubmit} className='loginForm'>
           
            <i onClick={()=>history.push('/')} class="arrow"></i>
            
            <p>Realize seu login</p>
            <div>
                <label htmlFor="email">Email ou CPF:</label>
                <input type="email" name="email" id="email" onChange={onChange} value={values.email} />
            </div>
            <div>
                <label htmlFor="password">Senha:</label>
                <input type="password" name="password" id="password" onChange={onChange} value={values.password} />
            </div>
            <Button buttonClass='blueBackground' buttonWord='Entrar'/>
            <a onClick={openPasswordRec}>Esqueci minha senha</a>
        </form>
    )
}

export default LoginForm
