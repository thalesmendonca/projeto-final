import React, {useState} from 'react'
import Divider from '../../../components/Divider/Divider'
import Button from '../../../components/Button/Button'

import './ForgetPasswordForm.css'


function ForgetPasswordForm() {
    return (
        <form className='forgetPasswordForm'>
            <p>Esqueci minha senha</p>
            <Divider />
            <label htmlFor="email">Email para recuperação</label>
            <input type="text" name="email" id="email" />
            <Button buttonClass='greyBackground' buttonWord='Enviar' />
        </form>
    )
    
}

export default ForgetPasswordForm
