import React, {useState} from 'react'
import Header from '../../components/Header/Header'
import LoginForm from './LoginForm/LoginForm'
import FogertPasswordForm from './ForgetPasswordForm/ForgetPasswordForm'

import Modal from '@material-ui/core/Modal'

import './LoginPage.css'

import backgroundImage from './assets/imgLogin.png'

function LoginPage() {
    /****** CONDITIONAL FOR PASSWORD RECOVERY FORM *****/
    const [openPasswordRec, setOpenPasswordRec] = useState(false)

    const handleOpen = () => {
        setOpenPasswordRec(true);
    };
    
      const handleClose = () => {
        setOpenPasswordRec(false);
    };

    return (
        <body className='loginPage'>
            <Header headerClass='onlyLogo'/>
            <img className='backgroundImage' src={backgroundImage} alt="" />
            <LoginForm  openPasswordRec={handleOpen}/>
            <Modal
                open={openPasswordRec}
                onClose={handleClose}
            >
                <FogertPasswordForm/>
            </Modal>
        </body>
    )
}

export default LoginPage
