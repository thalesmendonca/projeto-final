import React, {useState} from 'react'
import HeaderRestaurantLogged from '../../components/HeaderRestaurantLogged/HeaderRestaurantLogged'
import ManagementItem from '../../components/ManagementItem/ManagementItem'
import TitlePages from '../../components/TitlePages/TitlePages'
import AddPlate from './AddPlate/AddPlate'
import Footer from '../../components/Footer/Footer'

import './RestaurantManagement.css'
import {IoMdAddCircleOutline} from 'react-icons/io'

import Modal from '@material-ui/core/Modal'

function RestaurantManagement() {
    const [openAddPlate, setOpenAddPlate] = useState(false)

    const handleOpen = () => {
        setOpenAddPlate(true);
    };
    
      const handleClose = () => {
        setOpenAddPlate(false);
    };

    return (
        <div>
            <HeaderRestaurantLogged />
            <div className='bigBox'>
                <TitlePages title='Gestão de Restaurante' />
                <div className='smallBox'>
                    <div>
                        <ManagementItem />
                    </div>
                    <a className='linkAddPlate' onClick={handleOpen}>Adicionar Prato <IoMdAddCircleOutline/></a>
                </div>
            </div>
            <Modal
                open={openAddPlate}
                onClose={handleClose}
            >
                <AddPlate closeAddPlate={handleClose}/>
            </Modal>
            <Footer/>
        </div>
    )
}

export default RestaurantManagement