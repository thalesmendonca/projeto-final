import React from 'react'
import Divider from '../../../components/Divider/Divider'
import Button from '../../../components/Button/Button'

import './AddPlate.css'

function AddPlate({closeAddPlate}) {
    return (
        <form className='addPlate'>
            <div className='rowDiv'>
                <h4>Criar novo prato</h4>
                <a><h4 onClick={closeAddPlate}>x</h4></a>
            </div>
            <Divider />
            <div>
                <label htmlFor="">Nome</label>
                <input type="text" name="" id="" />
                <label htmlFor="">Foto</label>
                <input className='imgInput'  type="image" name="" id="" />
                <label htmlFor="">Descrição</label>
                <input type="text" name="" id="" />
                <label htmlFor="">Valor</label>
                <input type="text" name="" id="" />
                <div className='selectDiv'>
                    <label htmlFor="">Porção</label>
                    <select name="" id=""></select>
                </div>
                <Button buttonClass='blueBackground' buttonWord='Criar'/>
            </div>
        </form>
    )
}

export default AddPlate
