import React from 'react'
import HeaderUserLogged from '../../components/HeaderUserLogged/HeaderUserLogged'
import TitlePages from '../../components/TitlePages/TitlePages'
import UserEditProfileForm from './UserEditProfileForm/UserEditProfileForm'
import Footer from '../../components/Footer/Footer'
import './UserEditProfile.css'
function UserEditProfile() {
    return (
        <div className='userEditProfilePage'>
            <HeaderUserLogged headerClass='userLogged'/>
            <div className='bigBox'>
                <TitlePages title='Editar Dados' withArrow='withArrow'/>
                <UserEditProfileForm />
            </div>
            <Footer />
        </div>
    )
}

export default UserEditProfile
