import React from 'react'
import Button from '../../../components/Button/Button'

import './UserEditProfileForm.css'

function UserEditProfileForm() {
    return (
        <form className='userEditProfileForm'>
            <div>
                <label htmlFor="name">Nome</label>
                <input type="text" name="name" id="name" />
            </div>
            <div>
                <label htmlFor="email">E-mail</label>
                <input type="email" name="email" id="email" />
            </div>
            <div>
                <label htmlFor="cpf">CPF</label>
                <input type="text" name="cpf" id="cpf" />
            </div>
            <div className='doubleInput'>
                <div>
                    <label htmlFor="bithday">Data de nascimento</label>
                    <input type="date" name="birthday" id="birthday" />
                </div>
                <div>
                    <label htmlFor="phone">Telefone</label>
                    <input type="tel" name="phone" id="phone" />
                </div>
            </div>
            <div className='buttonsField'>
                <Button buttonClass='yellowBackground' buttonWord='Excluir Perfil'/>
                <Button buttonClass='yellowBackground'buttonWord='Editar'/>
            </div>
        </form>
    )
}

export default UserEditProfileForm
