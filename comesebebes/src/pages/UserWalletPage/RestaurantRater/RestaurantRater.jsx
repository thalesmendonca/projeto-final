import React, {useState} from 'react'
import Divider from '../../../components/Divider/Divider'
import Button from '../../../components/Button/Button'

import './RestaurantRater.css'
import RestaurantRate from '../../../components/RestaurantCards/RestaurantRate/RestaurantRate'

function RestaurantRater({closeRestaurantRater}) {
    return (
        <div className='restaurantRater'>
            <div>
                <div>
                    <h3>Como deseja avaliar Restaurante 1?</h3>
                    <Divider />
                </div>
                <div>
                    <RestaurantRate />
                    <Button onClick={closeRestaurantRater} buttonClass='yellowBackground' buttonWord='Avaliar'/>
                </div>
            </div>          
        </div>
    )
}

export default RestaurantRater
