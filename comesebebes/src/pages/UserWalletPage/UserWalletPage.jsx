import React, {useState} from 'react'
import HeaderUserLogged from '../../components/HeaderUserLogged/HeaderUserLogged'
import TitlePages from '../../components/TitlePages/TitlePages'
import Footer from '../../components/Footer/Footer'
import GeneralCard from '../../components/GeneralCard/GeneralCard'

import './UserWalletPage.css'
import RestaurantRater from './RestaurantRater/RestaurantRater'

import Modal from '@material-ui/core/Modal'


function UserWalletPage() {
    const [rateRestaurant, setRateRestaurant] = useState(false)

    const handleOpen = () => {
        setRateRestaurant(true);
    };
    
      const handleClose = () => {
        setRateRestaurant(false);
    };

    return (
        <>
            <HeaderUserLogged headerClass='userLogged'/>
            <div className='bigBox'>
                <TitlePages title='Saldo Disponível:' balance='R$ 80,00'withArrow='withArrow'/>
                <div className='smallBox'>
                    <h3>Histórico de compras</h3>
                    <div className='cardsDiv'>
                        <GeneralCard title='Coxinha de Jaca' description='Restaurant 1' userType='user' openRestaurantRater={handleOpen} value='R$ 8,00'/>
                        <GeneralCard title='Coxinha de Jaca' description='Restaurant 1' userType='user' openRestaurantRater={handleOpen} value='R$ 8,00'/>
                        <GeneralCard title='Coxinha de Jaca' description='Restaurant 1' userType='user' openRestaurantRater={handleOpen} value='R$ 8,00'/>
                        <GeneralCard title='Coxinha de Jaca' description='Restaurant 1' userType='user' openRestaurantRater={handleOpen} value='R$ 8,00'/>
                        <GeneralCard title='Coxinha de Jaca' description='Restaurant 1' userType='user' openRestaurantRater={handleOpen} value='R$ 8,00'/>
                        <GeneralCard title='Coxinha de Jaca' description='Restaurant 1' userType='user' openRestaurantRater={handleOpen} value='R$ 8,00'/>
                    </div>
                </div>
            </div>
            <Modal
                open={rateRestaurant}
                onClose={handleClose}
            >
                <RestaurantRater />
            </Modal>
            <Footer />            
        </>
    )
}

export default UserWalletPage
