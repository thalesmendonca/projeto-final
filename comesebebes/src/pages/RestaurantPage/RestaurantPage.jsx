import React from 'react'
import HeaderUserLogged from '../../components/HeaderUserLogged/HeaderUserLogged'
import Divider from '../../components/Divider/Divider'
import ItemCard from './ItemCard/ItemCard'
import Footer from '../../components/Footer/Footer'

import logoteste from './ItemCard/assets/logorestaurante.png'

import './RestaurantPage.css'
function RestaurantPage() {
    return (
        <div>
            <HeaderUserLogged headerClass='userLogged'/>
            <div className='bigBox'>
                <div className='bigBoxHeader'>
                    <div className='restaurantDescription'>
                        <h1>Restaurante 1</h1>
                        <p>Comida Vegana</p>
                        <p>Dono: Amanda Melo</p>
                        <p>Aberto das 8h às 16h</p>
                    </div>
                    <div className='circularImage'>
                        <img src={logoteste} alt="" />
                    </div>
                </div>
                <Divider/>
                <div className='itemSearch'>
                    <input placeholder='Pesquisa pelo nome' type="search" name="" id="" />
                </div>
                <div className='itensDiv'>
                    <ItemCard name='Coxinha de jaca' price='8,00' />
                    <ItemCard name='Coxinha de jaca' price='8,00' />
                    <ItemCard name='Coxinha de jaca' price='8,00' />
                    <ItemCard name='Coxinha de jaca' price='8,00' />
                    <ItemCard name='Coxinha de jaca' price='8,00' />
                    <ItemCard name='Coxinha de jaca' price='8,00' />
                    <ItemCard name='Coxinha de jaca' price='8,00' />
                    <ItemCard name='Coxinha de jaca' price='8,00' />
                    <ItemCard name='Coxinha de jaca' price='8,00' />
                    <ItemCard name='Coxinha de jaca' price='8,00' />
                    <ItemCard name='Coxinha de jaca' price='8,00' />
                    <ItemCard name='Coxinha de jaca' price='8,00' />
                    <ItemCard name='Coxinha de jaca' price='8,00' />
                    <ItemCard name='Coxinha de jaca' price='8,00' />
                    <ItemCard name='Coxinha de jaca' price='8,00' />
                </div>
            </div>
            <Footer />
        </div>
    )
}

export default RestaurantPage
