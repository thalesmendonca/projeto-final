import React from 'react'

import coxinha from './assets/coxinhadejaca.jpg'

import './ItemCard.css'

function ItemCard({image ,name, price}) {
    return (
        <div className='itemCard'>
            <div className='imgDiv'>
                <img src={coxinha} alt="" />
            </div>
            <div>
                <p>{name}</p>
                <p>R${price}</p>
            </div>
            <button className='circleButton'>+</button>
        </div>
    )
}

export default ItemCard
