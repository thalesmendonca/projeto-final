import React from 'react'
import HeaderAdminLogged from '../../components/HeaderAdminLogged/HeaderAdminLogged'
import TitlePages from '../../components/TitlePages/TitlePages'
import RestaurantOwnerRegisterForm from './RestaurantOwnerRegisterForm/RestaurantOwnerRegisterForm'
import Footer from '../../components/Footer/Footer'

function AdminRestaurantOwnerRegister() {
    return (
        <>
            <HeaderAdminLogged/>   
            <div className='bigBox'>
                <TitlePages title='Cadastro de Donos de Restaurante'/>
                <div className='smallBox'>
                    <RestaurantOwnerRegisterForm />
                </div>
            </div>
            <Footer />
        </>
    )
}

export default AdminRestaurantOwnerRegister
