import React from 'react'
import Button from '../../../components/Button/Button'

import './RestaurantOwnerRegisterForm.css'

function RestaurantOwnerRegisterForm() {
    return (
        <form className='RORF'>
            <label htmlFor="">Nome</label>
            <input type="text" name="" id="" />
            <label htmlFor="">E-mail</label>
            <input type="email" name="" id="" />
            <label htmlFor="">CPF</label>
            <input type="text" name="" id="" />
            <div className='rowDiv'>
                <div>
                    <label htmlFor="">Data de nascimento</label>
                    <input type="date" name="" id="" />
                </div>
                <div>
                    <label htmlFor="">Telefone</label>
                    <input type="tel" name="" id="" />
                </div>
            </div>
            <label htmlFor="">Endereço</label>
            <input type="text" name="" id="" />
            <Button buttonClass='blueBackground' buttonWord='Cadastrar'/>
        </form>
    )
}

export default RestaurantOwnerRegisterForm
