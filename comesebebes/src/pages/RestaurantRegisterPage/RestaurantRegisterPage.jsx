import React from 'react'
import HeaderRestaurantLogged from '../../components/HeaderRestaurantLogged/HeaderRestaurantLogged'
import TitlePages from '../../components/TitlePages/TitlePages'
import RegisterRestaurantForm from './RegisterRestaurantForm/RegisterRestaurantForm'
import Footer from '../../components/Footer/Footer'

function AdminRegisterRestaurantPage() {
    return (
        <>
            <HeaderRestaurantLogged />
            <div className='bigBox'>
                <TitlePages title='Cadastro de Restaurante'/>
                <RegisterRestaurantForm />
            </div>
            <Footer />
        </>
    )
}

export default AdminRegisterRestaurantPage
