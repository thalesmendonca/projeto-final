import React from 'react'
import Button from '../../../components/Button/Button'

import './RegisterRestaurantForm.css'

function RegisterRestaurantForm() {
    return (
        <form className='registerRestaurantForm'>
            <label htmlFor="image">Logo</label>
            <input className='logoInput' type="image" src="" alt="" />
            <label htmlFor="text">Nome do Restaurante</label>
            <input type="text" name="" id="" />
            <label htmlFor="text">Tipo de comida principal</label>
            <input type="text" name="" id="" />
            <label htmlFor="text">CNPJ</label>
            <input type="text" name="" id="" />
            <div>
                <p>Horário de funcionamento</p>
                <div className='rowDiv'>
                    <div>
                        <label htmlFor="time">de</label>
                        <input type="time" name="" id="" />
                    </div>
                    <div>
                        <label htmlFor="time">Até</label>
                        <input type="time" name="" id="" />
                    </div>
                </div>
            </div>
            <label htmlFor="text">Endereço</label>
            <input type="text" name="" id="" />
            <Button buttonClass='blueBackground' buttonWord='Cadastrar'/>
        </form>
    )
}

export default RegisterRestaurantForm
