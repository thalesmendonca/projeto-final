import React, { useState } from 'react'
import Button from '../../../components/Button/Button'
import {useHistory} from 'react-router-dom'
import axios from 'axios'

import './RegisterForm.css'


function initialState() {
    return {
        name: '',
        email:'',
        cpf:'',
        birthdate:'',
        telephone:'',
        address:'',
        password:'',
        password_confirmation:''
    }
}

function RegisterForm() {
    const [user, setUser] = useState(initialState)

    function onChange(event){
        const { value, name } = event.target
        setUser({
            ...user,
            [name]:value
        })
    }

    
    async function register (user) {
        console.log(user)
        axios.post('https://comesandbebes.herokuapp.com/user/register', user)
        .then((res)=>{
            console.log(res)
        })
        .catch((err) => {
            console.log(err)
        })
    }
    
    

    function onSubmit(event) {
        event.preventDefault();
        register(user)
    }

    const history = useHistory()
    return (
        <form onSubmit={onSubmit} className='registerForm'>
            <i onClick={()=>history.push('/')} class="arrow"></i>
            <p>Cadastre-se</p>
            <div>
                <label htmlFor="name">Nome</label>
                <input type="text" name="name" id="name" onChange={onChange} value={user.name}/>
            </div>
            <div>
                <label htmlFor="email">E-mail</label>
                <input type="email" name="email" id="email" onChange={onChange} value={user.email}/>
            </div>
            <div>
                <label htmlFor="cpf">CPF</label>
                <input type="text" name="cpf" id="cpf" onChange={onChange} value={user.cpf}/>
            </div>
            <div className='rowDiv'>
                <div className='birthdayDiv'>
                    <label htmlFor="bithdate">Data de nascimento</label>
                    <input type="date" name="birthday" id="birthday" onChange={onChange} value={user.birthdate}/>
                </div>
                <div>
                    <label htmlFor="phone">Telefone</label>
                    <input type="tel" placeholder="99-99999-9999" pattern="[0-9]{2}-[0-9]{5}-[0-9]{4}"name="telephone" id="phone" onChange={onChange} value={user.telephone}/>
                </div>
            </div>
            <div>
                <label htmlFor="userType">Tipo de Usuário</label>
                <select name="userTyper" id="userType">
                    <option value="empty"></option>
                    <option value="deliveryman">Entregador</option>
                    <option value="client">Cliente</option>   
                </select> 
            </div>
            <div>
                <label htmlFor="adress">Endereço</label>
                <input type="text" name="address" id="adress" onChange={onChange} value={user.address}/>
            </div>
            <div className='rowDiv'>
                <div>
                    <label htmlFor="password">Senha:</label>
                    <input type="password" name="password" id="password" onChange={onChange} value={user.password}/>
                </div>
                <div>
                    <label htmlFor="passwordConfirm">Confirme a Senha:</label>
                    <input type="password" name="password_confirmation" id="confirm_password" onChange={onChange} value={user.password_confirmation}/>
                </div>
            </div>
            <Button buttonClass='yellowBackground' buttonWord='Cadastrar'/>
        </form>
    )
}

export default RegisterForm
