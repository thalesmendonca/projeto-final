import React from 'react'
import Header from '../../components/Header/Header'
import RegisterForm from './RegisterForm/RegisterForm'

import './RegisterPage.css'

function ResgisterPage() {
   
    return (
        <div className='registerPage'>
            <Header headerClass='onlyLogo'/>
            <RegisterForm />
        </div>
    )
}

export default ResgisterPage
