import React from 'react'
import Button from '../../../components/Button/Button'

import './RestaurantEditProfileForm.css'

function RestaurantEditProfileForm() {
    return (
        <form className='restaurantEditProfileForm'>
            <label htmlFor="">Nome</label>
            <input type="text" name="" id="" />
            <label htmlFor="">E-mail</label>
            <input type="email" name="" id="" />
            <label htmlFor="">CPF</label>
            <input type="text" name="" id="" />
            <div className='rowDiv'>
                <div>
                    <label htmlFor="">Data de Nascimento</label>
                    <input type="date" name="" id="" />
                </div>
                <div>
                    <label htmlFor="">Telefone</label>
                    <input type="tel" name="" id="" />
                </div>
            </div>
            
            <label htmlFor="">Endereço</label>
            <input type="text" name="" id="" />
            <div className='buttonsField'>
                <Button buttonClass='blueBackground' buttonWord='Excluir Perfil'/>
                <Button buttonClass='blueBackground' buttonWord='Editar'/>
            </div>
        </form>
    )
}

export default RestaurantEditProfileForm
