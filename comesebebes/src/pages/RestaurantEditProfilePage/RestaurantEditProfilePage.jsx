import React from 'react'
import HeaderRestaurantLogged from '../../components/HeaderRestaurantLogged/HeaderRestaurantLogged'
import TitlePages from '../../components//TitlePages/TitlePages'
import RestaurantEditProfileForm from './RestaurantEditProfileForm/RestaurantEditProfileForm'
import Footer from '../../components/Footer/Footer'

function RestaurantEditProfilePage() {
    return (
        <>
            <HeaderRestaurantLogged/>
            <div className='bigBox'>
                <TitlePages title='Editar dados' withArrow='withArrow'/>
                <div className='smallBox'>
                    <RestaurantEditProfileForm/>
                </div>
            </div>
            <Footer />
        </>
    )
}

export default RestaurantEditProfilePage
