import React, {useState, useEffect} from 'react'
import './UserLoggedPage.css'
import HeaderUserLogged from '../../components/HeaderUserLogged/HeaderUserLogged'
import RestaurantCards from '../../components/RestaurantCards/RestaurantCards'
import TitlePages from '../../components/TitlePages/TitlePages'
import SearchBar from '../../components/SearchBar/SearchBar'
import CartSideBar from '../../components/CartSidebar/CartSidebar'
import FilterDropDown from './FilterDropDown/FilterDropDown'

import {getRestaurantList} from '../../service/api'

function UserLoggedPage() {

    const [showCart, setShowCart] = useState(false)
    
    const [restaurantList, setRestaurantList] = useState([])
    useEffect(() => {
        getRestaurantList(setRestaurantList)
    }, [])

    return (
        <>
            <HeaderUserLogged headerClass='userLogged' setShowCart={setShowCart}/>
            <div className='userEditProfilePage'>
                <div className='bigBox'>
                    <TitlePages title='O que vamos pedir hoje?'/>
                    <div className='filter'>
                        <FilterDropDown />
                    </div>
                    <div className='catalog'>
                        {restaurantList.map((restaurant)=><RestaurantCards info={restaurant}/>)}
                        
                    </div>
                </div>
            </div>
        </>
    )
}

export default UserLoggedPage
