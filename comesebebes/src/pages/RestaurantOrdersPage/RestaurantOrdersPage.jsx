import React from 'react'
import HeaderRestaurantLogged from  '../../components/HeaderRestaurantLogged/HeaderRestaurantLogged'
import TitlePages from '../../components/TitlePages/TitlePages'
import GeneralCards from '../../components/GeneralCard/GeneralCard'

function RestaurantOrdersPage() {
    return (
        <div>
            <HeaderRestaurantLogged />
            <div className='bigBox'>
                <TitlePages title='Pedidos'/>
                <div className='smallBox'>
                    <GeneralCards title='Coxinha de Jaca' description='Status: Em preparo' userType='restaurant' value='Marcar como feito'/>
                    <GeneralCards title='Coxinha de Jaca' description='Status: Em preparo' userType='restaurant' value='Marcar como feito'/>
                    <GeneralCards title='Coxinha de Jaca' description='Status: Em preparo' userType='restaurant' value='Marcar como feito'/>
                </div>
            </div>
        </div>
    )
}

export default RestaurantOrdersPage
