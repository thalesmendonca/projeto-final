import React from 'react'
import './UserList.css'
import imgTrash from '../../../assets/delete.svg'

function AdminList() {
    return (
        <div>
            <div className='divList'>
                <div>
                    <h3 className='foodName'>Mairin</h3>
                    <p className="foodDescription">mairin@email.com</p>
                    <p className="portion">000.000.000-00</p>
                </div>
                <div className='icons'>
                    <a href=""><img src={imgTrash} alt="" /></a>
                </div>
            </div>
        </div>
    )
}

export default AdminList
