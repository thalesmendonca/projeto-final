import React from 'react'
import TitlePages from '../../components/TitlePages/TitlePages'
import HeaderAdminLogged from '../../components/HeaderAdminLogged/HeaderAdminLogged'
import UserList from './UserList/UserList'


function AdminPage() {
    return (
        <div>
            <HeaderAdminLogged />
            <div className="bigBox">
                <TitlePages title='Lista de Usuários' />
            </div>
            <UserList />
        </div>
    )
}

export default AdminPage