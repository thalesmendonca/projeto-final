import React from 'react'
import HeaderUserLogged from '../../components/HeaderUserLogged/HeaderUserLogged'
import TitlePages from '../../components/TitlePages/TitlePages'
import GeneralCards from '../../components/GeneralCard/GeneralCard'
import Footer from '../../components/Footer/Footer'

import './DeliverymanWalletPage.css'

function DeliverymanWalletPage() {
    return (
        <>
            <HeaderUserLogged headerClass='deliverymanLogged'/>
            <div className='bigBox'>
                <TitlePages title='Saldo Disponível:' balance='R$: 240,00' withArrow='withArrow'/>
                <div className='smallBox'>
                    <h4>Histórico de entregas</h4>
                    <div>
                        <GeneralCards title='Coxinha de jaca' description='Restaurante 1' userType='deliverymanReceiving' value='R$32,00'/>
                        <GeneralCards title='Coxinha de jaca' description='Restaurante 1' userType='deliverymanReceiving' value='R$32,00'/>
                        <GeneralCards title='Coxinha de jaca' description='Restaurante 1' userType='deliverymanReceiving' value='R$32,00'/>
                        <GeneralCards title='Coxinha de jaca' description='Restaurante 1' userType='deliverymanReceiving' value='R$32,00'/>
                    </div>
                </div>
            </div>
            <Footer />
        </>
    )
}

export default DeliverymanWalletPage
