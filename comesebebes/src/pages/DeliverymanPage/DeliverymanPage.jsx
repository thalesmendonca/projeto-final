import React, {useState} from 'react'
import HeaderUserLogged from '../../components/HeaderUserLogged/HeaderUserLogged'
import TitlePages from '../../components/TitlePages/TitlePages'
import GeneralCard from '../../components/GeneralCard/GeneralCard'
import UnityDelivery from './UnityDelivery/UnityDelivery'
import Footer from '../../components/Footer/Footer'

import Modal from '@material-ui/core/Modal'

function DeliverymanPage() {
    const [openUnityDelivery, setOpenUnityDelivery] = useState(false)

    const handleOpen = () => {
        setOpenUnityDelivery(true);
    };
    
      const handleClose = () => {
        setOpenUnityDelivery(false);
    };

    return (
        <div>
            <HeaderUserLogged headerClass='deliverymanLogged'/>
            <div className='bigBox'>
                <TitlePages title='Entregas Disponíveis'/>
                <div className='smallBox'>
                    <GeneralCard title='Restaurante 1' description='Rua dos bobos, número 0' openUnityDelivery={handleOpen} userType='deliveryman' value='Aceitar Entrega'/>
                    <GeneralCard title='Restaurante 1' description='Rua dos bobos, número 0' openUnityDelivery={handleOpen} userType='deliveryman' value='Aceitar Entrega'/>
                    <GeneralCard title='Restaurante 1' description='Rua dos bobos, número 0' openUnityDelivery={handleOpen} userType='deliveryman' value='Aceitar Entrega'/>
                    <GeneralCard title='Restaurante 1' description='Rua dos bobos, número 0' openUnityDelivery={handleOpen} userType='deliveryman' value='Aceitar Entrega'/>
                </div>
            </div>
            <Modal
                open={openUnityDelivery}
                onClose={handleClose}
            >
                <UnityDelivery closeUnityDelivery={handleClose}/>
            </Modal>
            <Footer />
        </div>
    )
}

export default DeliverymanPage
