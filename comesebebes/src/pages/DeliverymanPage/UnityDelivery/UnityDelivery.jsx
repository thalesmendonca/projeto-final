import React from 'react'
import Divider from '../../../components/Divider/Divider'
import Button from '../../../components/Button/Button'
import GeneralCard from '../../../components/GeneralCard/GeneralCard'
import './UnityDelivery.css'

function UnityDelivery({closeUnityDelivery}) {
    return (
        <div className='unityDelivery'>
            <div>
                <div className='titleDiv'>
                    <h3>Entrega Unitária</h3>
                    <a onClick={closeUnityDelivery}>x</a>
                </div>
                <Divider />
                <div className='scrollableDiv'>
                    <div className='insideScrollable'>
                        <div>
                            <h3>Restaurante</h3>
                            <div className='rowDiv'>
                                <p>Restaurante 1</p>
                                <p>|</p>
                                <p>00.000.000/0000-00</p>
                            </div>
                            <p>Rua dos bobos, número 0</p>
                        </div>
                        <Divider />
                        <div>
                            <h3>Cliente</h3>
                            <div className='rowDiv'>
                                <p>Mairon</p>
                                <p>|</p>
                                <p>(00) 00000-0000</p>
                            </div>
                            <p>Rua dos outros bobos, outro numero que não é 0</p>
                        </div>
                        <Divider />
                        <div>
                            <h3>Pedido</h3>
                            <GeneralCard title='Coxinha de Jaxa' userType='user' value='R$8,00'/>
                            <GeneralCard title='Coxinha de Jaxa' userType='user' value='R$8,00'/>
                            <GeneralCard title='Coxinha de Jaxa' userType='user' value='R$8,00'/>
                            <GeneralCard title='Coxinha de Jaxa' userType='user' value='R$8,00'/>
                        </div>
                        <Divider/>
                        <div>
                            <div>
                                <p>Total</p>
                                <p>R$ 32,00</p>
                            </div>
                            <div>
                                <p>Você receberá</p>
                                <p className='toBeEarned'>R$ 6,40</p>
                            </div>
                        </div>
                    </div>
                </div>
            <Button buttonClass='blueBackground' buttonWord='Finalizar Entrega'/>
            </div>
        </div>
    )
}

export default UnityDelivery
