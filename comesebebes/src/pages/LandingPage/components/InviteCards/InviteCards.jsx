import React from 'react'

import './InviteCards.css'

function InviteCards({image, imageDescription, invite, link}) {
    return (
       <>
            <div className='inviteCard'>
                <img src={image} alt={imageDescription} />
                <p>{invite}</p>
                <a href={link}>
                    <i class="arrow"></i>
                </a>
            </div>
        </>
    )
}

export default InviteCards