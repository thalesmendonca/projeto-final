import React, {useState, useEffect} from 'react'
import Header from '../../components/Header/Header'
import Button from '../../components/Button/Button'
import Divider from '../../components/Divider/Divider'
import InviteCards from './components/InviteCards/InviteCards'
import RestaurantCard from '../../components/RestaurantCards/RestaurantCards'
import Footer from '../../components/Footer/Footer'
import { getRestaurantList } from '../../service/api'

import './LandingPage.css'

import deliveryman from './assets/imgDeliveryman.png'
import client from './assets/imgClient.png'

function LandingPage() {
    
    const [restaurantList, setRestaurantList] = useState([])
    useEffect(() => {
        getRestaurantList(setRestaurantList)
    }, [])

    return (
        <>
            <div className='banner'>
                <Header />
                <h1>Comes&Bebes</h1>
                <h3>O serviço que tem a cara da sua fome</h3>
            </div>
            <div className='invitationalDiv'>
                <InviteCards image={client} imageDescription='pessoa comendo pizza' invite='Está com fome? Crie sua conta.'/>
                <InviteCards image={deliveryman} imageDescription='entregador com pizzas em mãos' invite='É entregador? Faça seu cadastro.'/>
            </div>
            <div className='restaurants'>
                <Divider />
                <h2>Conheça nossos restaurantes</h2>
                <div className='exRestaurants'>
                    {restaurantList.map((restaurant)=>
                        <RestaurantCard info={restaurant} />
                    )}
                </div>
            </div>
            <Footer />
        </>
    )
}

export default LandingPage
