import React from 'react'
import HeaderAdminLogged from '../../components/HeaderAdminLogged/HeaderAdminLogged'
import TitlePages from '../../components/TitlePages/TitlePages'
import DeliverymanList from './DeliverymanList/DeliverymanList'

function AdminPage() {
    return (
        <div>
            <HeaderAdminLogged />
            <div className="bigBox">
                <TitlePages title='Admissão de Entregadores' />
            </div>
            <DeliverymanList />
        </div>
    )
}

export default AdminPage