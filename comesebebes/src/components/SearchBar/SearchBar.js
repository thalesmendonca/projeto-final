import React, {useState} from 'react'
import Jsondata from '../../MOCK_DATA.json'
import './SearchBar.css'



function SearchBar() {
    const [searchRestaurant, setSearchRestaurant] = useState('')
    return (
    <>
        {console.log(Jsondata)}
        <input type="text" placeholder="Pesquisar restaurantes..." onChange={event => {setSearchRestaurant(event.target.value)}}/>
        {Jsondata.filter((val)=>{
            if (searchRestaurant == "") {
                return val
            } else if (val.name.toLowerCase().includes(searchRestaurant.toLowerCase())) {
                return val
            }
        }).map((val, key)=>{
            return (
                <div className="restaurant" key={key}>
                    <p>{val.name}</p>
                </div>
            );
        })}
    </>
    )
}

export default SearchBar;