import React from 'react'
import Divider from '../Divider/Divider'

import './TitlePages.css'

function TitlePages({title, withArrow,balance}) {
    return (
        
        <>
            <div className='titlePages'>
                <div>
                    <i className={withArrow}></i>
                    <h1>{title}</h1>
                </div>
                <p className='balance'>{balance}</p>
            </div>
            <Divider />
        </>
    
    )
}

export default TitlePages
