import React from 'react'
import logo from '../../assets/logo1.png'
import { useHistory } from 'react-router-dom'

import './HeaderAdminLogged.css'

function HeaderAdminLogged() {
        const history = useHistory()
        return (
        <div className={'headerAdminLogged'}>
            <a onClick={()=>history.push('/')}>
                <img src={logo} alt="" />
            </a>
            <nav>
                <ul>
                    <li><a onClick={()=>history.push('/admin-page-user-list')}>Lista de Usuários</a></li>
                    <li><a onClick={()=>history.push('/admin-page-deliveryman-list')}>Admissão de Entregadores</a></li>
                    <li><a onClick={()=>history.push('/admin-page-owner-register')}>Cadastrar dono de restaurante</a></li>
                </ul>
            </nav>
        </div>
    )
}

export default HeaderAdminLogged
