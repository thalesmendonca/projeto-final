import React from 'react'
import Button from '../Button/Button'
import {useHistory} from 'react-router-dom'

import './Header.css'

import logo from '../../assets/logo1.png'

function Header({headerClass}) {
    const history = useHistory()
    return (
        <header className={headerClass}>
            <a onClick={()=> history.push('/')}>
                <img src={logo} alt="" />
            </a>
            <nav>
                <Button buttonWord='Cadastre-se' buttonClass='whiteBackground' clicking={()=> history.push('/register')}/>
                <Button buttonWord='Entrar' buttonClass='yellowBackground' clicking={()=> history.push('/login')}/>    
            </nav>
        </header>
    )   
}

export default Header
