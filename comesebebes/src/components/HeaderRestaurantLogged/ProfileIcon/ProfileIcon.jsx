import React from 'react'
import profileIcon from './assets/user1.svg'
import './ProfileIcon.css'

function ProfileIcon() {
    return (
        <>
            <img src={profileIcon} className="profileIcon" alt="" />
        </>
    )
}

export default ProfileIcon
