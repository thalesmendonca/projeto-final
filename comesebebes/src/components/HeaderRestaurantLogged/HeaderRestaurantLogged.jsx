import React from 'react'
import logo from '../../assets/logo1.png'
import ProfileIcon from './ProfileIcon/ProfileIcon'
import {useHistory} from 'react-router-dom'

import './HeaderRestaurantLogged.css'

function HeaderRestaurantLogged() {
    const history = useHistory()
    return (
        <div className='headerRestaurantLogged'>
            <a onClick={()=>history.push('/')}>
                <img src={logo} alt="" />
            </a>
            <nav>
                <ul>
                    <li><a onClick={()=>history.push('/restaurant-management')}>Gestão de Restaurante</a></li>
                    <li><a onClick={()=>history.push('/restaurant-stock')}>Gestão de Estoque</a></li>
                    <li><a onClick={()=>history.push('/restaurant-results')}>Resultados</a></li>
                    <li><a onClick={()=>history.push('/restaurant-orders')}>Pedidos</a></li>
                </ul>
            </nav>
            <a href="">
                <ProfileIcon />
            </a>
        </div>
    )
}

export default HeaderRestaurantLogged
