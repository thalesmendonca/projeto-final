import React from 'react'

import './Button.css'

function Button({ buttonWord ,buttonClass, clicking }) {
    return (
        <button className={buttonClass} onClick={clicking}>
            {buttonWord}
        </button>
    )
}

export default Button
