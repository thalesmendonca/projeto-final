import React from 'react'
import RestaurantRate from './RestaurantRate/RestaurantRate'

import './RestaurantCards.css'

import image from '../../assets/restaurant-logo-teste.png'

function RestaurantCards({info}) {
    return (
        <div className='restaurantCard'>
            <img src={image} alt="" />
            <div className='infoField'>
                <h3>{info['name']}</h3>
                <p>{info['foodtype']}</p>
                <RestaurantRate />
            </div>
        </div>
    )
}

export default RestaurantCards
