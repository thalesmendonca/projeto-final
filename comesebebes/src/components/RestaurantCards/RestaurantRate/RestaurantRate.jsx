import React from 'react'
import './RestaurantRate.css'

function RestaurantRate({}) {
    return (
        <div>
            <span class="fa fa-star checked"></span>
            <span class="fa fa-star checked"></span>
            <span class="fa fa-star checked"></span>
            <span class="fa fa-star"></span>
            <span class="fa fa-star"></span>
        </div>
    )
}

export default RestaurantRate
