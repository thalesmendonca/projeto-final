import React from 'react'
import './MarketIcon.css'
import marketIcon from './assets/market-icon.png'


function MarketIcon() {
    return (
        <>
            <img src={marketIcon} className="marketIcon" alt="" />
        </>
    )
}

export default MarketIcon
