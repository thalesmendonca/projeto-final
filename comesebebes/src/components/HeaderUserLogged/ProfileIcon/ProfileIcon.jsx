import React from 'react'
import profileIcon from './assets/user1.svg'
import './ProfileIcon.css'

function ProfileIcon({clicking}) {
    return (
        <>
            <img onClick={clicking} src={profileIcon} className="profileIcon" alt="" />
        </>
    )
}

export default ProfileIcon
