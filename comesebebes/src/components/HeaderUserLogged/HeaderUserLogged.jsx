import React, {useEffect, useState} from 'react'
import {useHistory} from 'react-router-dom'
import './HeaderUserLogged.css'
import logo from '../../assets/logo1.png'
import ProfileIcon from './ProfileIcon/ProfileIcon'
import marketIcon from './MarketIcon/assets/market-icon.png'
import MenuDropDown from './MenuDropDown/MenuDropDown'

function HeaderUserLogged({headerClass, setShowCart}) {
    const history = useHistory()

    return (
        <header className={headerClass}>
            <a onClick={()=> history.push('/')}>
                <img src={logo} alt="" />
            </a>
            <nav>
                    <a href="">
                        <MenuDropDown /> 
                    </a>
                    <a href="" className='' onClick={(e)=>{
                        e.preventDefault()
                        setShowCart(true)
                        }}>
                        <img src={marketIcon} alt="" />
                    </a>
            </nav>
        </header>
    )   
}

export default  HeaderUserLogged
