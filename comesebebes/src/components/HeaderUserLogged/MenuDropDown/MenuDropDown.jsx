import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import ProfileIcon from '../ProfileIcon/ProfileIcon';
import { BsWallet, BsGear, BsBoxArrowInRight } from 'react-icons/bs'
import Divider from '../../Divider/Divider'
import { useHistory } from 'react-router-dom';


import './MenuDropDown.css'

const StyledMenu = withStyles({
  paper: {
    border: '1px solid #d3d4d5',
  },
})((props) => (
  <Menu
    elevation={0}
    getContentAnchorEl={null}
    anchorOrigin={{
      vertical: 'bottom',
      horizontal: 'center',
    }}
    transformOrigin={{
      vertical: 'top',
      horizontal: 'center',
    }}
    {...props}
  />
));

const StyledMenuItem = withStyles((theme) => ({
  root: {
    '&:focus': {
      '& .MuiListItemIcon-root, & .MuiListItemText-primary': {
      },
    },
  },
}))(MenuItem);

export default function CustomizedMenus() {

    let history = useHistory()
    const [anchorEl, setAnchorEl] = React.useState(null);

    const handleClick = (event) => {
        event.preventDefault()
        setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };

    return (
        <div>
            <ProfileIcon clicking={handleClick}/>
        
            <StyledMenu
                id="customized-menu"
                anchorEl={anchorEl}
                keepMounted
                open={Boolean(anchorEl)}
                onClose={handleClose}
            >
                <div className='menuTitle'>
                    <p>Olá, Mairon</p>
                    <Divider/>
                </div>
                <StyledMenuItem onClick={()=>history.push('/user-wallet')}>
                <ListItemIcon>
                    <BsWallet />
                </ListItemIcon>
                <ListItemText primary="Carteira" />
                </StyledMenuItem>
                <StyledMenuItem onClick={()=>history.push('/user-edit-profile')}>
                <ListItemIcon>
                    <BsGear />
                </ListItemIcon>
                <ListItemText primary="Editar dados" />
                </StyledMenuItem>
                <StyledMenuItem>
                <ListItemIcon>
                    <BsBoxArrowInRight />
                </ListItemIcon>
                <ListItemText primary="Sair" />
                </StyledMenuItem>
            </StyledMenu>
        </div>
  );
}
