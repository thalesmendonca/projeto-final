import React from 'react'
import GeneralCard from '../GeneralCard/GeneralCard'
import Divider from '../Divider/Divider'
import Button from '../Button/Button'

import './CartSidebar.css'

function CartSidebar() {
    return (
        <div className='cartSidebar'>
            <div>
                <h3>Carrinho</h3>
                <div>
                    <GeneralCard title='Coxinha de Jaxa' description='Restaurante 1' value='R$ 8,00'/>
                    <GeneralCard title='Coxinha de Jaxa' description='Restaurante 1' value='R$ 8,00'/>
                    <GeneralCard title='Coxinha de Jaxa' description='Restaurante 1' value='R$ 8,00'/>
                </div>
                <p className='emptyCart'>Limpar carrinho</p>
            </div>
            <div>
                <Divider />
                <div className='total'>
                    <div>Total</div>
                    <div>R$ 32,00</div>
                </div>
                <Button buttonClass='yellowBackground' buttonWord='Pagar'/>
            </div>
        </div>
    )
}

export default CartSidebar
