import React from 'react'
import './ManagementItem.css'
import imgTeste from './assets/coxinhadejaca.jpg'
import imgTeste2 from '../../assets/delete.svg'

function ManagementItem() {
    return (
        <div>
            <div className='item'>
                <div>
                    <img src={imgTeste} alt="" className="imgDiv"/>
                </div>
                <div>
                    <h3 className='foodName'>Coxinha de Jaca</h3>
                    <p className="foodDescription">Uma deliciosa opção de coxinha vegana</p>
                    <p className="portion">Porção: Média</p>
                    <p className="value">R$ 8,00</p>
                </div>
                <div className='iconDiv'>
                    <a href="" ><img src={imgTeste2} alt="" className="icon"/></a>
                </div>
            </div>

        </div>
    )
}

export default ManagementItem
