import React from 'react'

import './GeneralCard.css'

function GeneralCard({ title, description,openRestaurantRater, openUnityDelivery, userType, value}) {
    return (
        <div className='generalCard'>
            <div>
                <h4>{title}</h4>
                <p 
                    className='description'
                    onClick={userType == 'user' ? openRestaurantRater : ''}
                >{description}</p>
            </div>
            <p onClick={userType == 'deliveryman' ? openUnityDelivery : ''} className={userType}>{value}</p>
        </div>
    )
}

export default GeneralCard
