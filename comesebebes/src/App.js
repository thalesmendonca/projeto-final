import React from 'react'
import './styles/Reset.css'
import Routes from './routes/Routes'
import './styles/Root.css'

function App() {
  return (
    <div className="App">
        <Routes/>

    </div>
  );
}

export default App;
